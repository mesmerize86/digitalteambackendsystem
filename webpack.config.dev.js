const webpack = require('webpack');
const path = require('path');

import ExtractTextPlugin from 'extract-text-webpack-plugin'

export default {
  entry: [
    'webpack-hot-middleware/client?reload=true',
    './client/index.js',
    './themes/digitalteam/client/sass/main-sky.scss'
  ],
  output: {
    path: '/',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.txt$/,
        use: 'raw-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: 'file-loader'
      },
      {
        test: /.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!resolve-url-loader!sass-loader?sourceMap',
          publicPath: '/'
        })
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader'
      },
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, 'server/shared/validation'),
          path.resolve(__dirname, 'client')
        ],
        exclude: [/node_modules/],
        use: [
        {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }]
      }
    ]
  },
  node: {
    fs: 'empty'
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, 'app'),
      path.resolve(__dirname, 'images')
    ],
    extensions: ['.js']
  },
  devtool: 'eval-source-map',
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      filename: 'style.min.css',
      disable: false,
      allChunks: true
    })
  ]
}