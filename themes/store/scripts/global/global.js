
$(document).ready(function(){
    global.init();
});

var global = {
    init: function(){
        global.overlapLabel();
    },

    overlapLabel: function(){
            $(".js-overlap").on("focus blur", "input", function(e) {
            $(this).closest(".js-overlap").toggleClass("isFocused", e.type === "focusin" || this.value.length > 0);
        });
    }
}
