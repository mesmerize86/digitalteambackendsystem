(function($){
    $.fn.themeSlider = function(options){
        var defaultOptions = {
            variationAppliedTo: 'themePanel',
            slide: 'themePanelPallete',
            layer: 1,
            dock: 'topRight',
            isExpanded: false
        };
        
        options = $.extend({}, defaultOptions, options);
        
        var sliderConfig = {
            init: function($element){
                var self = this;
                self.$element = $element;
                self.$pallete = $('.' + options.slide);
                self.$element.addClass(options.variationAppliedTo + '--' + options.dock);
                
                self.$pallete.on('click', function(e){
                    e.stopImmediatePropagation();
                });
                
                self.$element.on('click', function(){
                    self.click();
                })
            },
            setupDimension: function(){
                var self = this;

                if(options.dock == 'rightTop' || options.dock == 'rightBottom' ||
                options.dock == 'leftTop' || options.dock == 'leftBottom') {
                    if(!options.isExpanded){
                        self.$element.width(((80 * options.layer) + 40) + 'px'); // need smart logic on this
                    }
                    else{
                        self.$element.width(0);
                    }
                        
                    options.isExpanded = !options.isExpanded;
                }
            },
            click: function(){
                var self = this;
                self.$pallete.fadeToggle('fast', function(){
                    self.$element.toggleClass('themePanel-is-expanded');
                    self.setupDimension();
                });
            }
        };
        
        return this.each(function(){
            var $this = $(this);
            var thisConfig = Object.create(sliderConfig);
            
            thisConfig.init($this);
        });
    };
})(jQuery);