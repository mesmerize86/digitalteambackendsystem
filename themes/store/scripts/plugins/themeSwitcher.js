(function($){
    $.fn.themeSwitcher = function(target){
        var switchTheme = function(e){
            var $this = $(this);
            if(target !== undefined)
            {
                $('link[id="' + target + '"]').attr("href", $this.data('theme'));
            }
            
            e.stopImmediatePropagation();
        };
        
        return this.each(function(){
            var $this = $(this);
            $this.find('li[data-theme]').click(switchTheme);
        });
    };
})(jQuery);