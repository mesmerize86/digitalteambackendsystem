
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
    path.join(__dirname, './client/index.js'),
    path.join(__dirname, './themes/digitalteam/client/sass/main.scss')
  ],
  output: {
    path: path.join(__dirname, '/build'),
    publicPath: path.join(__dirname, '/build')
  },
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: 'file-loader?name=../images/[name].[ext]'
      },
      {
        test: /.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!resolve-url-loader!sass-loader?sourceMap',
          publicPath: '/'
        })
      },
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, 'server/shared/validation'),
          path.resolve(__dirname, 'client')
        ],
        exclude: [/node_modules/],
        use: [
        {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }]
      }
    ]
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, 'app')
    ],
    extensions: ['.js']
  },
  node: {
    fs: 'empty'
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      filename: './css/default.min.css',
      disable: false,
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: 'server/index.html',
      chunkSortMode: 'dependency'
    })
  ]
}