import React from 'react';
import { Link } from 'react-router';

class Sidebar extends React.Component {
  render () {
    return(     
        <div className="sidebar">
          <ul className="sidebar-nav">
            <li><Link to="/" className="sidebar-nav-link">Automate Single Bottle</Link></li>
            <li><Link to="/case-builder" className="sidebar-nav-link">Case Builder</Link></li>
            <li><Link to="/design/typography" className="sidebar-nav-link">Multi Page Banner</Link></li>
            <li><Link to="/design/typography" className="sidebar-nav-link">Homepage Banner</Link></li>
            <li><Link to="/design/typography" className="sidebar-nav-link">Homepage Tiles</Link></li>
            <li><Link to="/search-response-code" className="sidebar-nav-link">Search Cases with Response Code</Link></li>
            <li><Link to="/design/typography" className="sidebar-nav-link">Search Cases with BOM Code</Link></li>
            <li><Link to="/design/typography" className="sidebar-nav-link">Search Single Bottles</Link></li>
          </ul>
        </div>
    );
  }
}

export default Sidebar;
