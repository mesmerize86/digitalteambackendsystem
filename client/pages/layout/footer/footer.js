import React from 'react';

class Footer extends React.Component {
  render () {
    return(
      <footer className='Footer'>
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12'>
                <p className='text-center'>
                Created By: Kishor Maharjan<br/>
                Version: 1.0.0<br/>
              &copy; Copyright 2017| Digital Team Backend | Date: 13/07/2017</p>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
