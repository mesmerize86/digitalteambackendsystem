import React from 'react';

import TopBar from './top-bar/top-bar';

class Header extends React.Component {
  render () {
    return(
      <header>
        <TopBar />
      </header>
    );
  }
}

export default Header;
