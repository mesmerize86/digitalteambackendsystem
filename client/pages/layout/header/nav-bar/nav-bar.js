import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';

class NavBar extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      collapse : false
    };
  }
  onClickCollapseMenu(e){
    e.preventDefault();
    this.setState({ collapse: !this.state.collapse });
  }
  render () {
    return(
      <nav className='nav'>
        <div className='container'>
          <div className='navbar-header'>
            <button type='button' className='navbar-toggle collapsed' onClick={this.onClickCollapseMenu.bind(this)}>
              <span className='sr-only'>Toggle navigation</span> Menu <i className='fa fa-bars'></i>
            </button>
          </div>
          
          <div className={classnames('collapse navbar-collapse', { 'in' : this.state.collapse})}>
            <ul className='navbar navbar-nav navbar-right'>
              <li>
                <Link to='/' className='navbar-link'>Home</Link>
              </li>
              <li>
                <Link to='/' className='navbar-link'>Login</Link>
              </li>
              <li>
                <Link to='/signup' className='navbar-link'>Register</Link>
              </li>
              <li>
                <Link to='/pattern-library' className='navbar-link'>Pattern-Library</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
