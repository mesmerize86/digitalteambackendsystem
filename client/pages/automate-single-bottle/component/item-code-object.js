import React from 'react';

const itemCodes = (id, itemCode) => {
  return (
  {
    id : id,
    ItemCode : itemCode
  }
  );
};

itemCodes.propTypes = {
  id: React.PropTypes.number.isRequired,
  itemCode: React.PropTypes.string.isRequired
};

export default itemCodes;