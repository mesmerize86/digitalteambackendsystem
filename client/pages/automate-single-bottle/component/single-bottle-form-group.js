import React from 'react';

const SingleBottleFormGroup = ({index, id, field, value, error, type, onChange}) => {
  return(
      <span>{index}
      <input 
        type={type}
        id={id}
        className="form-control" 
        value={value} 
        name={field} 
        onChange={onChange}/>
      {error && <span className="help-block">{error}</span>}
      </span>
  );
};

SingleBottleFormGroup.propTypes = {
  index: React.PropTypes.number.isRequired,
  field: React.PropTypes.string.isRequired,
  error: React.PropTypes.string,
  onChange: React.PropTypes.func.isRequired,
  type: React.PropTypes.string.isRequired
};

SingleBottleFormGroup.defulatProps = {
  type: 'text'
};

export default SingleBottleFormGroup;