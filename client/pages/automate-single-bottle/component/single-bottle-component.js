import React from 'react';
import SingleBottleFormGroup from './single-bottle-form-group';
import itemCodeObject from './item-code-object';

class SingleBottleComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      textFields : [],
      itemCodes: [],
      itemCodesList : []
    };
  }
  
  addTextField(){
    const textFields = this.state.textFields.concat(SingleBottleFormGroup);
    this.setState({ 
      textFields
    });
  }
  
  isValid(){
    const textFields = this.state.textFields;
    const itemCodes = this.state.itemCodesList;
    return (textFields.length === itemCodes.length) ? true : false;
  }
  
  removeTextField(index){
    this.state.textFields.splice(index, 1);
    this.state.itemCodesList.splice(index, 1);
    this.setState({ textFields: this.state.textFields });
  }
  
  changeHandler(e){ 
    const itemCodesList = this.state.itemCodesList;
    itemCodesList[e.target.id] = itemCodeObject(e.target.id, e.target.value);
    this.setState({ itemCodesList });
  }
  
  generateHandler(){
    //console.log(this.state.itemCodesList);
    if(!this.isValid()){
      this.props.addFlashMessage({
        type:'error',
        text: 'Some of the fields are missing. !!! Please correct it.'
      });
    }else{
      const userData = this.state.itemCodesList;
      this.props.singleBottleRequest(userData).then((response)=>{
        this.props.addFlashMessage({
          type: 'success',
          text: 'Your file has been successfully generated.'
        });
        setTimeout(()=>{
          window.location.reload();
        }, 5000);
      });
    }
  }
  
  render () {
    const textFields = this.state.textFields.map((Element, index) =>{
      return (
        <div className="form form-inline" key={index}>
          <div className="form-group">
            <SingleBottleFormGroup
              index={index + 1}
              id={index}
              type="text"
              field={`itemCode_${index + 1}`}
              value={this.state.itemCodes[index]}
              onChange={this.changeHandler.bind(this)}
              />
            <button className="button button-danger" onClick={ this.removeTextField.bind(this, index)}>Remove</button>
          </div>  
        </div>
      );
    });
    return(
      <div>
        <button onClick = {this.addTextField.bind(this)} className="button button-primary">Add a Bottle ( itemCode )</button>
        <button onClick = {this.generateHandler.bind(this)} className="button button-primary">Generate List</button>
        <div className="singleBottleContainer">
          { textFields }
        </div>
      </div>
    );
  }
}

SingleBottleComponent.propTypes = {
  addFlashMessage : React.PropTypes.func.isRequired,
  singleBottleRequest: React.PropTypes.func.isRequired
};

export default SingleBottleComponent;