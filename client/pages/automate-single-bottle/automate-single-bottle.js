import React from 'react';
import SingleBottleComponent from './component/single-bottle-component';
import { connect } from 'react-redux';
import { addFlashMessage } from '../../components/flash-messages/flashMessageAction';
import { singleBottleRequest } from '../../data/singleBottleAPI/singleBottleRequestAction';
import FlashMessageList from '../../components/flash-messages/flashMessagesList';

class AutomateSingleBottle extends React.Component {
  render () {
    const { addFlashMessage, singleBottleRequest } = this.props;
    return(
      <div className="automateSingleBottle Page">
        <div className="col-xs-12">
          <h1 className="text-center">Automate Single Bottle</h1>
          <p>This will help you generate single bottle image in a batch file. Please follow the steps below.</p>

          <p><strong>Step 1: </strong> Click on Add Button to select single bottle images. <br/><strong>Note:</strong> You can select only one image at a time. Click on add button again to add more images on your list.</p>
          <p><strong>Step 2: </strong> Click on Generate List Button to generate a json file for you.</p>
        </div>
        <div className="col-xs-12">
          <FlashMessageList />
        </div>
        <div className="col-xs-12">
          <SingleBottleComponent addFlashMessage={addFlashMessage} singleBottleRequest={singleBottleRequest}/>
        </div>
      </div>
    );
  }
}

AutomateSingleBottle.propTypes = {
  addFlashMessage: React.PropTypes.func.isRequired,
  singleBottleRequest: React.PropTypes.func.isRequired
};

export default connect(null, {addFlashMessage, singleBottleRequest})(AutomateSingleBottle);
