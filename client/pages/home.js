import React from 'react';
import LoginForm from './components/form/login/login';

class Home extends React.Component {
  render () {
    return(
      <section className='homePage Page'>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-6 hidden-xs'> 
              <h1>Something will go here.</h1>
            </div>
            <div className='col-xs-12 col-sm-6'>
              <LoginForm />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Home;
