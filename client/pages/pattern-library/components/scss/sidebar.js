import React from 'react';
import { Link } from 'react-router';

class Sidebar extends React.Component {
  render () {
    return(
      <div className='sidebar'>
        <label>SCSS Components</label>
        <ul className='sidebar-nav'>
          <li><Link to='/design/color-elements' className='sidebar-nav-link'>SMACSS</Link></li>
          <li><Link to='/design/color-elements' className='sidebar-nav-link'>Folder Structure</Link></li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
