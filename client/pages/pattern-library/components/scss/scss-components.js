import React from 'react';

import Sidebar from './sidebar';

class SCSSComponent extends React.Component {
  render () {
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-3 hidden-xs'>
            <Sidebar />
          </div>
          <div className='col-sm-9 col-xs-12'>
            <div className='content'>
              <h1>SCSS Component</h1>
              <p>Everytime when your project gets bigger and bigger, we all developers have an issue to maintain the css file. However, with the SCSS partial features, it has help some extend to maintain the file, if you have good file structure.</p> 
              <p>Here I have listed one of the best <a href='https://smacss.com/' target='_blank'>SCSS Architecture</a> which I found online and have been using it in all my project. With the help of this architecture, <strong>now I can find the scss file just by doing inspect element.</strong></p>
              <h3>Folder Structure</h3>
              <p>I have a root folder called scss_source. Inside this folder, I have another folder which is called store. This folder has got all default styling</p>
              <p>For better understanding, visit <a href='https://smacss.com/' target='_blank' alt='SMACSS Architecture'> SMACSS Architecture</a></p>
              <p><strong>NAMESPACING</strong></p>
              <p>I am using a namespacing that is based off the&nbsp;BEM&nbsp;front-end naming methodology which stands for: Block, Element, Modifyer. The naming convention follows the following pattern:</p>
              <p>.block{} <br/>

              The point of BEM is to tell other developers more about what a piece of markup is doing from its name alone.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SCSSComponent;
