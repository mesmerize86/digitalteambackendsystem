import React from 'react';

import classnames from 'classnames';

class ThemePanel extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hideThemePanel: false
    };
  }
  
  toggleThemeSlider(e){
    e.preventDefault();
    this.setState({ hideThemePanel: !this.state.hideThemePanel});
  }
  
  changeTheme(themename){
    const style = document.getElementById('styles');
    style.setAttribute('href', themename);
  }
  
  render () {
    return(
      <div className={classnames('themePanel themePanel--rightTop', { 'isThemPanelHidden' : this.state.hideThemePanel})} data-title='Themes' onClick={this.toggleThemeSlider.bind(this)}>
          <ul className='themePanelPallete'>
              <li className='themePanelPalleteItem--default' onClick={this.changeTheme.bind(this, '/assets/css/defaulttheme.min.css')}>Default</li>
              <li className='themePanelPalleteItem--sky' onClick={this.changeTheme.bind(this, '/assets/css/expensetracker.min.css')}>Expense Tracker</li>
              <li className='themePanelPalleteItem--blood' onClick={this.changeTheme.bind(this, '/assets/css/defaulttheme.min.css')}>Blood</li>
          </ul>
      </div>
    );
  }
}

export default ThemePanel;