import React from 'react';
import Sidebar from './sidebar';

class DesignComponent extends React.Component {
  render () {
    return(
      <div className='container'>
        <div className='row'>
          <div className='col-sm-3 hidden-xs'>
            <Sidebar />
          </div>
          <div className='col-sm-9 col-xs-12'>
            <div className='content'>
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DesignComponent;
