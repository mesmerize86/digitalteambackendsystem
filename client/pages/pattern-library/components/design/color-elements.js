import React from 'react';

class ColorElements extends React.Component {
  render () {
    return(
        <div className='content'>
          <h3>Colour Palette</h3>
          <p>This is the primary and secondary colour palettes as described in the PSD styleguide and in theme.sass</p>
          <div className='panel panel-default'>
            <div className='panel-body'>
              <h4>Mono Palette</h4>
              <span className='swatch swatch--mono-xxxlight'>$color-mono-xxxlight</span>
              <span className='swatch swatch--mono-xxlight'>$color-mono-xxlight</span>
              <span className='swatch swatch--mono-xlight'>$color-mono-xlight</span>
              <span className='swatch swatch--mono-light'>$color-mono-light</span>
              <span className='swatch swatch--mono'>$color-mono</span>
              <span className='swatch swatch--mono-dark'>$color-mono-dark</span>
              <span className='swatch swatch--mono-xdark'>$color-mono-xdark</span>
              <span className='swatch swatch--mono-xxdark'>$color-mono-xxdark</span>
              <span className='swatch swatch--mono-xxxdark'>$color-mono-xxxdark</span>
              
              <h4>Colour Palette</h4>
                <span className='swatch swatch--red'>$theme-color-red</span>
                <span className='swatch swatch--cherry'>$theme-color-cherry</span>
                <span className='swatch swatch--rose'>$theme-color-rose</span>
                <span className='swatch swatch--jam'>$theme-color-jam</span>
                <span className='swatch swatch--garnet'>$theme-color-garnet</span>
                <span className='swatch swatch--crimson'>$theme-color-crimson</span>
                <span className='swatch swatch--ruby'>$theme-color-ruby</span>
                <span className='swatch swatch--scarlet'>$theme-color-scarlet</span>
                <span className='swatch swatch--blood'>$theme-color-blood</span>
                <span className='swatch swatch--currant'>$theme-color-currant</span>
                <span className='swatch swatch--blush'>$theme-color-blush</span>
                <hr/>
                <span className='swatch swatch--green'>$theme-color-green</span>
                <span className='swatch swatch--chartreuse'>$theme-color-chartreuse</span>
                <span className='swatch swatch--juniper'>$theme-color-juniper</span>
                <span className='swatch swatch--sage'>$theme-color-sage</span>
                <span className='swatch swatch--olive'>$theme-color-olive</span>
                <span className='swatch swatch--lime'>$theme-color-lime</span>
                <span className='swatch swatch--emerald'>$theme-color-emerald</span>
                <span className='swatch swatch--seafoam'>$theme-color-seafoam</span>
                <span className='swatch swatch--parakeet'>$theme-color-parakeet</span>
                <span className='swatch swatch--basil'>$theme-color-basil</span>
                <span className='swatch swatch--pine'>$theme-color-pine</span>
                <hr/>
                <span className='swatch swatch--tan'>$theme-color-tan</span>
                <span className='swatch swatch--macaroon'>$theme-color-macaroon</span>
                <span className='swatch swatch--granola'>$theme-color-granola</span>
                <span className='swatch swatch--eggnog'>$theme-color-eggnog</span>
                <span className='swatch swatch--sepia'>$theme-color-sepia</span>
                <span className='swatch swatch--latte'>$theme-color-latte</span>
                <span className='swatch swatch--oyster'>$theme-color-oyster</span>
                <span className='swatch swatch--hazelnut'>$theme-color-hazelnut</span>
                <span className='swatch swatch--parmesean'>$theme-color-parmesean</span>
                <span className='swatch swatch--buttermilk'>$theme-color-buttermilk</span>
                <span className='swatch swatch--sandcastle'>$theme-color-sandcastle</span>
                
            <h4>Type</h4>
            <span className='swatch swatch--body-copy'>$theme-color-body-copy</span>
            </div>
          </div>
      </div>
    );
  }
}

export default ColorElements;
