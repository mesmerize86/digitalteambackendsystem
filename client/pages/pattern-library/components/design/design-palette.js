import React from 'react';

class Themes extends React.Component {
  render () {
    return(
        <div className='content'>
          <h3>Design Palette</h3>
          <div className='panel panel-default'>
            <div className='panel-body'>
              <h3>Expense Tracker Theme</h3>
                <span className='swatch swatch--primary'>$theme-color-primary</span>
                <span className='swatch swatch--secondary'>$theme-color-secondary</span>
                <span className='swatch swatch--tertiary'>$theme-color-tertiary</span>
                <span className='swatch swatch--quaternary'>$theme-color-quaternary</span>
                <span className='swatch swatch--quinary'>$theme-color-quinary</span>
                <span className='swatch swatch--senary'>$theme-color-senary</span>
                <span className='swatch swatch--septenary'>$theme-color-septenary</span>
            </div>
          </div>
      </div>
    );
  }
}

export default Themes;
