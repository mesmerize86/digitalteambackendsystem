import React, { PropTypes } from 'react';

class Button extends React.Component {
  render () {
    return(
      <div>
      <h1>Button</h1>
      <div className='panel panel-default'>
        <div className='panel-body'>
          <div className='button-group'>
            <button className='button button-default'> default </button>
          </div>
          <div className='button-group'>
            <button className='button button-primary'> primary </button>
          </div>
          <div className='button-group'>
            <button className='button button-info'> info </button>
          </div>
          <div className='button-group'>
            <button className='button button-warning'> warning </button>
          </div>
          <div className='button-group'>
            <button className='button button-success'> success </button>
          </div>
          <div className='button-group'>
            <button className='button button-danger'> danger </button>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default Button;
