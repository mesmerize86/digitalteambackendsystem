import React from 'react';
import Sidebar from './sidebar';

class UIComponents extends React.Component {
  render () {
    return(
      <div className='container'>
        <div className='row'>
          <div className='col-sm-3 hidden-xs'>
            <Sidebar />
          </div>
          <div className='col-sm-9 col-xs-12'>
            <div className='content'>
              <h1>Pattern Library</h1>
              <p>Welcome to the Expense Tracker Pattern Library. Keep in mind that Expense Tracker uses the <a href='http://getbootstrap.com/css/' target='_blank'>Bootstrap 3</a> grid framework. Bootstrap components generally aren't included in this library so please refer to <a href='http://getbootstrap.com/css/' target='_blank'>Bootstrap 3</a> docs for futher information.</p>

              <h3>How  &lt;Pre> Works </h3>
              <p>HTML’s pre element is a simple and semantic way of displaying formatted content (such as source code) but it does have a few quirks.</p>
              <p>If you want to represent a block of source code in your HTML document, you should use a code element nested inside a pre element. This is semantic, and functionally, it gives you the opportunity to tell search engine crawlers, social media apps, RSS readers, and other interoperating web services that the content is computer code. <a href='http://www.sitepoint.com/everything-need-know-html-pre-element/'>Click here</a> for more details. </p>
              <pre><code>&lt;pre&gt;
&lt;code&gt;
  &lt;div>HTML Markup Goes Here &lt;/div&gt;
&lt;/code&gt;
&lt;/pre&gt;</code></pre>
              <p>Fortunately, there are many tools that will help you do this. There’s, for instance, the <a href='http://www.freeformatter.com/html-escape.html' target='_blank'>Free Online HTML Escape Tool</a>.</p>
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UIComponents;
