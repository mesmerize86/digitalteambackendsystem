import React from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';


class Navigation extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      collapse : false,
      navTitle : 'Design Element'
    };
  }
  
  onClickCollapseMenu(e){
    e.preventDefault();
    this.setState({ collapse: !this.state.collapse });
  }
  
  onClickSetTitle(title){
    this.setState({
      navTitle : title
    });
  }
  render () {
    return(
      <nav className='nav'>
        <div className='container'>
          <div className='navbar-header'>
            <button type='button' className='navbar-toggle collapsed' onClick={this.onClickCollapseMenu.bind(this)}>
              <span className='sr-only'>Toggle navigation</span> Menu <i className='fa fa-bars'></i>
            </button>
            <a className='navbar-brand page-scroll' href='#page-top'>{this.state.navTitle}</a>
          </div>
          
          <div className={classnames('collapse navbar-collapse', { 'in' : this.state.collapse})}>
            <ul className='navbar navbar-nav navbar-right'>
              <li>
                <Link to='/components/design/design-components' className='navbar-link' onClick={this.onClickSetTitle.bind(this, 'Design Elements')}>Design Elements</Link>
              </li>
              <li>
                <Link to='/components/ui/ui-components' className='navbar-link' onClick={this.onClickSetTitle.bind(this, 'UI-Components')}>UI-Components</Link>
              </li>
              <li>
                <Link to='/components/js/js-components' className='navbar-link' onClick={this.onClickSetTitle.bind(this, 'JS-Components')}>JS-Components</Link>
              </li>
              <li>
                <Link to='/components/scss/scss-components' className='navbar-link' onClick={this.onClickSetTitle.bind(this, 'SCSS')}>SCSS</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navigation;
