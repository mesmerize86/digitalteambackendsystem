import React, { PropTypes } from 'react';

class Header extends React.Component {
  render () {
    return(
      <header className='Header'>
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12'>
              <h2>#performance matter</h2>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;