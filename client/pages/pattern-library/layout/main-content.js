import React from 'react';

class MainContent extends React.Component {
  render () {
    return(
    <section className='section'>
        {this.props.patternLibraryNav}
      </section>
    );
  }
}

export default MainContent;
