import React from 'react';

import ColorElements from '../components/design/color-elements';
import Typography from '../components/design/typography';

class Content extends React.Component {
  render () {
    return(
      <div>
        <ColorElements />
        <Typography />
      </div>
    )
  }
}

export default Content
