import React from 'react';

import Header from './pattern-library/layout/header';
import Navigation from './pattern-library/layout/navigation';
import MainContent from './pattern-library/layout/main-content';
import ThemePanel from './pattern-library/components/theme-panel/theme-panel';

class PatternLibrary extends React.Component {

  render () {
    return(
      <div className='patternLibraryPage Page'>
        <ThemePanel />
        <div className='wrapper'>
          <Header />
          <Navigation />
          <MainContent patternLibraryNav={this.props.children} />
        </div>
      </div>
    );
  }
}

export default PatternLibrary;
