import React from 'react';
import FormGroup from '../../../../components/form-group';

class Login extends React.Component {
  constructor(props){
    super(props);
    this.state={
      username : '',
      password: ''
    };
  }
  
  onChange(e){
    this.setState({
      [e.target.name] : e.target.value
    });
  }
  
  onSubmit(e){
    e.preventDefault();
    console.log(this.state.username);
  }
  
  render () {
    return(
      <div>
        <h1>Login Form</h1>
        <form onSubmit={this.onSubmit.bind(this)} className='form'>
          <FormGroup
            label='username/email'
            field='username'
            value = {this.state.username}
            placeholder = 'username/email'
            onChange={this.onChange.bind(this)}
            />
          <FormGroup
            label='password'
            field='password'
            value = {this.state.password}
            onChange={this.onChange.bind(this)}
            />
          <button type='submit' className='button button-primary button-block'>Login</button>
        </form>
      </div>
    );
  }
}

export default Login;
