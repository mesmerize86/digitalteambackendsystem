import React from 'react';
import FormGroup from '../../../../components/form-group';
import validateInput from '../../../../../server/shared/validation/signup';

class Signup extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: '',
      email: '',
      secretQuestion: '',
      secretAnswer: '',
      errors : {},
      isLoading: false
    };
  }
  
  onChange(e){
    this.setState({
      [e.target.name] : e.target.value
    });
  }
  
  isValid(){
    const { errors, isValid } = validateInput(this.state);
    
    if(!isValid){
      this.setState({ errors, isLoading: false});
    }
    return isValid;
  }
  
  onSubmit(e){
    e.preventDefault();
    //clear the errors first when you click on submit
    this.setState({ errors: {}, isLoading: true });
    
    //action always return promise
    if(this.isValid()){
      this.props.userSignupRequest(this.state).then(
        () => {
          this.props.addFlashMessage({
            type:'success',
            text: 'You signed up successfully. Please Login now.'
          });
          this.setState({ isLoading: false });
          // this.context.router.push('/')
        }
      )
      .catch(error => {
        this.setState({ errors: error.response.data, isLoading: false });
      });
    }
  }
  
  render () {
    const { errors } = this.state;
    return(
      <div>
        <h4>Sign Up</h4>
        <form onSubmit={this.onSubmit.bind(this)} className='form'>
          <FormGroup
            label='Username'
            value={this.state.username}
            field='username'
            onChange={this.onChange.bind(this)}
            error={errors.username}
            />
          <FormGroup
            label='Password'
            value={this.state.password}
            field='password'
            type='password'
            onChange={this.onChange.bind(this)}
            error={errors.password}
            />
          <FormGroup
            label='Confirm Password'
            value={this.state.confirmPassword}
            field='confirmPassword'
            type='password'
            onChange={this.onChange.bind(this)}
            error={errors.confirmPassword}
            />
          <FormGroup
            label='First Name'
            value={this.state.firstName}
            field='firstName'
            onChange={this.onChange.bind(this)}
            error={errors.firstName}
            />
          <FormGroup
            label='Last Name'
            value={this.state.lastName}
            field='lastName'
            onChange={this.onChange.bind(this)}
            error={errors.lastName}
            />
          <FormGroup
            label='Email'
            value={this.state.email}
            field='email'
            onChange={this.onChange.bind(this)}
            error={errors.email}
            />
          <FormGroup
            label='Secret Question'
            value={this.state.secretQuestion}
            field='secretQuestion'
            onChange={this.onChange.bind(this)}
            error={errors.secretQuestion}
            />
          <FormGroup
            label='Secret Answer'
            value={this.state.secretAnswer}
            field='secretAnswer'
            onChange={this.onChange.bind(this)}
            error={errors.secretAnswer}
            />
          <button type='submit' className='button button-primary button-block' disabled={this.state.isLoading}>Register</button>
        </form>
      </div>
    );
  }
}

Signup.propTypes = {
  userSignupRequest : React.PropTypes.func.isRequired,
  addFlashMessage : React.PropTypes.func.isRequired
};

Signup.contextTypes = {
  router : React.PropTypes.object.isRequired
};
export default Signup;
