import React from 'react';

const TextFieldComponent = ({field, value, type, onChange}) => {
  return (
    <input
      type={type}
      name={field}
      value={value}
      onChange={onChange}
      className = "form-control"
    />
  );
};

TextFieldComponent.propTypes= {
  type : React.PropTypes.string.isRequired,
  field : React.PropTypes.string.isRequired,
  value : React.PropTypes.string.isRequired,
  onChange : React.PropTypes.func.isRequired
};

TextFieldComponent.defaultProps = {
  type : 'text'
};

export default TextFieldComponent;
