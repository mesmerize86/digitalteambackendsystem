import React from 'react';
import Header from './layout/header/header';
import Sidebar from './layout/sidebar/sidebar';
import Footer from './layout/footer/footer';

class Index extends React.Component {
  render () {
    return(
      <div className="wrapper">
        <Header />
        <section className="section">          
          <Sidebar />
          { this.props.children }
        </section>
        <Footer />
      </div>
    );
  }
}

export default Index;
