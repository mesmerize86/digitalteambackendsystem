import React from 'react';
import Signup from './components/form/signup/signup';
import { connect } from 'react-redux';
import { userSignupRequest } from '../data/signup/userSignupRequestAction';
import { addFlashMessage }from '../components/flash-messages/flashMessageAction';
import FlashMessageList from '../components/flash-messages/flashMessagesList';

class Register extends React.Component {
  render () {
    const { userSignupRequest, addFlashMessage } = this.props;

    return(
      <section className='registerPage page'>
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12'>
              <FlashMessageList />
            </div>
          </div>
          <div className='row'>
            <div className='col-xs-12 col-sm-5 hidden-xs'>
              <h4>Register a new account</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
            </div>
            <div className='col-xs-12 col-sm-7'>
              <Signup userSignupRequest={userSignupRequest} addFlashMessage={addFlashMessage}/>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

Register.propTypes = {
  userSignupRequest: React.PropTypes.func.isRequired
};
export default connect(null, { userSignupRequest, addFlashMessage })(Register);
