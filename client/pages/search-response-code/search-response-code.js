import React from 'react';
import SearchResponseCodeComponent from './component/search-response-code-component';
import { connect } from 'react-redux';
import { addFlashMessage } from '../../components/flash-messages/flashMessageAction';
import { searchResponseCodeRequest} from '../../data/searchResponseCodeAPI/searchResponseCodeAction';
import FlashMessageList from '../../components/flash-messages/flashMessagesList';


class SearchResponseCode extends React.Component {
  render () {
    const { addFlashMessage, searchResponseCodeRequest } = this.props;
    return(
      <div className="searchResponseCode Page">
          <h1 className="text-center">Search with Response Code</h1>
          <div className="container">
            <div className="row">
              <FlashMessageList />
              <SearchResponseCodeComponent addFlashMessage={addFlashMessage} searchResponseCodeRequest={searchResponseCodeRequest}/>
            </div>
          </div>
      </div>
    );
  }
}

SearchResponseCode.propTypes = {
  addFlashMessage : React.PropTypes.func.isRequired,
  searchResponseCodeRequest : React.PropTypes.func.isRequired
};

export default connect(null, {addFlashMessage, searchResponseCodeRequest})(SearchResponseCode);
