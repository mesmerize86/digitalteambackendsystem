import React from 'react';
import TextFieldComponent from '../../components/form-group/textfield-component';
import BrandConfiguration from '../../../data/brandConfiguration/brand-configuration';

class SearchResponseCodeComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      responseCode : '',
      brandName : ''
    };
  }

  handleChangeResponseCode(e){
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  handleChangeBrand(e){
    this.setState({
      brandName : e.target.value
    });
  }

  handleSearchResponseCodeSubmit(e){
    e.preventDefault();
    const responseCode = this.state.responseCode;
    const brandName = this.state.brandName;
    if(this.isValid()){
      // this.props.searchResponseCodeRequest('https://www.winepeople.com.au', 3095010).then(response => {
      //   console.log(response);
      // });
      console.log(this.props);
    }else{
      this.props.addFlashMessage({
        type: 'error',
        text: 'You missed to select one option below. Please correct it and search again.'
      });
    }
  }

  isValid(){
    const responseCode = this.state.responseCode;
    const brandName = this.state.brandName;
    console.log(responseCode, brandName);
    return ((!brandName) || (responseCode < 1) ? false : true );
  }

  render () {
    const options = BrandConfiguration.map(brand => {
      return (
        <option key={brand.name} value={brand.brandURL}>{brand.name}</option>
      );
    });

    return(
      <form className="form form-inline" onSubmit={this.handleSearchResponseCodeSubmit.bind(this)}>
        <TextFieldComponent
          type="text"
          field="responseCode"
          value = {this.state.responseCode}
          onChange={this.handleChangeResponseCode.bind(this)}
        />
        <select className="form-control" defaultValue="" onChange={this.handleChangeBrand.bind(this)}>
          <option value="" disabled>Please select the brand</option>
          {options}
        </select>
        <button type="submit" className="button button-primary">Search</button>
      </form>
    );
  }
}

SearchResponseCodeComponent.propTypes={
  addFlashMessage : React.PropTypes.func.isRequired
};

export default SearchResponseCodeComponent;
