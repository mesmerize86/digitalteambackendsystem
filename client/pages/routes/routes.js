import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Index from '../index';
import AutomateSingleBottle from '../automate-single-bottle/automate-single-bottle';
import CaseBuilder from '../case-builder/case-builder';
import SearchResponseCode from '../search-response-code/search-response-code';
import Home from '../home';
import Signup from '../register';


import PatternLibrary from '../pattern-library';
// UI Component
import UIComponents from '../pattern-library/components/ui/ui-components';
import FormGroupComponent from '../pattern-library/components/ui/form-group';
import ButtonComponent from '../pattern-library/components/ui/button';

// JS Component
import JSComponents from '../pattern-library/components/js/js-components';
import DocumentComponent from '../pattern-library/components/js/document';

// SCSS Component
import SCSSComponents from '../pattern-library/components/scss/scss-components';

// Design Component
import DesignComponent from '../pattern-library/components/design/design-components';
import ColorElements from '../pattern-library/components/design/color-elements';
import Typography from '../pattern-library/components/design/typography';
import DesignPalette from '../pattern-library/components/design/design-palette';


import PageNotFound from '../page-not-found';

export default (
  <Route path='/' component={Index}>
    <IndexRoute component={AutomateSingleBottle} />
    <Route path='/case-builder' component={CaseBuilder} />
    <Route path='/search-response-code' component={SearchResponseCode}>
      <IndexRoute component={UIComponents} />
      <Route path='/components/design/design-components' component={DesignComponent}>
        <IndexRoute component={ColorElements} />
        <Route path='/components/design/design-palette' component={DesignPalette} />
        <Route path='/components/design/color-elements' component={ColorElements} />
        <Route path='/components/design/typography' component={Typography} />
      </Route>
      <Route path='/components/ui/ui-components' component={UIComponents}>
        <IndexRoute component={FormGroupComponent} />
        <Route path='/components/ui/form-group' component={FormGroupComponent} />
        <Route path='/components/ui/button' component={ButtonComponent} />
      </Route>
      <Route path='/components/js/js-components' component={JSComponents}>
        <IndexRoute component={DocumentComponent} />
        <Route path='/components/js/document' component={DocumentComponent} />
      </Route>
      <Route path='/components/scss/scss-components' component={SCSSComponents}></Route>
    </Route>
  </Route>
);
