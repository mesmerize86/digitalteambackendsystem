import axios from 'axios';

export function singleBottleRequest(userData){
  return dispatch => {
    return axios.post('/api/singleBottles', userData );
  };
}
