export default
[
  {
    'name' : 'Winepeople',
    'abbreviation' : 'wp',
    'brandURL' : 'https://www.winepeople.com.au',
    'path' : '/images/au/en/brands/wp/product/'
  },
  {
    'name' : 'The Australian Wine',
    'abbreviation' : 'adc',
    'brandURL' : 'https://www.theaustralianwine.com.au',
    'path' : '/images/au/en/brands/adc/product/'
  },
  {
    'name' : 'Virgin Wines',
    'abbreviation' : 'virgin',
    'brandURL' : 'https://www.virginwines.com.au',
    'path' : '/images/au/en/brands/virgin/product/'
  },
  {
    'name' : 'Laithwaites Wine',
    'abbreviation' : 'law',
    'brandURL' : 'https://www.laithwaiteswine.co.nz',
    'path' : '/images/nz/en/brands/wp/product/'
  }
];
