import { combineReducers } from 'redux';
import flashMessages from './components/flash-messages/flashMessageReducer';

export default combineReducers({
  flashMessages
});
