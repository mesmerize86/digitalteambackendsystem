import React from 'react';
import { connect } from 'react-redux';
import FlashMessage from './flashMessage';

class FlashMessageList extends React.Component {
  render () {
    const messages = this.props.messages.map(message =>
      <FlashMessage key={message.id} message = { message } />
    );
    return (
      <div>
        {messages}
      </div>
    );
  }
}

FlashMessageList.propTypes = {
  messages: React.PropTypes.array.isRequired //we need to pass this messages from store to this component, so we do it connect, mapStateToProps
};


function mapStateToProps(state){
  return {
    messages: state.flashMessages //flashMessages is define in rootReducer
  };
}

export default connect(mapStateToProps)(FlashMessageList);
