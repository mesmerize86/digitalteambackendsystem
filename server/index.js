import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from '../webpack.config.dev';
import webpackHotMiddleware from 'webpack-hot-middleware';

import singleBottles from './routes/singleBottles';

let app = express();

app.use(bodyParser.json());
app.use(express.static(__dirname + './'));
app.use('/api/singleBottles', singleBottles);

const compiler = webpack(webpackConfig);

app.use(webpackMiddleware(compiler, {
  hot: true,
  publicPath: webpackConfig.output.publicPath,
  noInfo: true
}));


app.use(webpackHotMiddleware(compiler));

app.get('/*', (req, res) => {
  res.sendFile(path.resolve(__dirname, './index.html'));
})

app.listen(3000, ()=> console.log(' running on local host: 3000'));
