import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data){
  const errors = {};
  
  if(validator.isEmpty(data.username)){
    errors.username = 'This field is required.';
  }
  if(validator.isEmpty(data.password)){
    errors.password = 'This field is required.';
  }
  if(validator.isEmpty(data.confirmPassword)){
    errors.confirmPassword = 'This field is required.';
  }
  
  if(validator.isEmpty(data.firstName)){
    errors.firstName = 'This field is required.';
  }
  if(validator.isEmpty(data.lastName)){
    errors.lastName = 'This field is required.';
  }
  if(validator.isEmpty(data.email)){
    errors.email = 'This field is required.';
  }
  if(validator.isEmpty(data.secretQuestion)){
    errors.secretQuestion = 'This field is required.';
  }
  if(validator.isEmpty(data.secretAnswer)){
    errors.secretAnswer = 'This field is required.';
  }
  return {
    errors,
    isValid : isEmpty(errors)
  };
}
