const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.common.js');

config.bail = true;
config.profile = true;

config.output = {
  path: path.resolve(__dirname, './build/assets'),
  publicPath: path.resolve(__dirname, './build/assets'),
  filename: './scripts/bundle.min.js'
}

config.plugins = config.plugins.concat([
  new webpack.LoaderOptionsPlugin({
    minimize:true,
    debug: false
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)}
  }),
  new webpack.optimize.UglifyJsPlugin({
    beautify: false,
    comments: false,
    compress: {
      warnings: false,
      screw_ie8: true
    }
  }),
])

config.module.loaders = config.module.rules.concat([
  {
  test: /\.js$/,
  include: [
    path.resolve(__dirname, './server/shared/validation'),
    path.resolve(__dirname, './client')
  ],
  exclude: [/node_modules/],
  use: [
  {
    loader: 'babel-loader',
    options: {
      presets: ['es2015']
    }
  }]
}
])
module.exports = config;